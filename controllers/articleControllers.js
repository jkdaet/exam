const Article = require("../models/Article")

module.exports.createArticle = (reqBody) => {
    let newArticle = new Article({
        title: reqBody.title,
        content: reqBody.content
    })

    return newArticle.save().then((article,err) => {
        if(err){
            return false
        }
        else{
            console.log(article)
            return true
        }
    })
}

module.exports.editArticle = (articleId,reqBody) => {
    let updateArticle = {
        title: reqBody.title,
        content: reqBody.content
    }

    return Article.findByIdAndUpdate(articleId, updateArticle).then((articleUpdate, error) => {
        if(error){
            return false
        }
        else{
            return true
        }
    })
}

module.exports.retrieveArticle = () => {
    return Article.find().then()
}

module.exports.deleteArticle = (articleId) => {
    return Article.findByIdAndDelete(articleId).then((article, err) => {
        if(err){
            return false
        }
        else{
            return true
        }
    })
}