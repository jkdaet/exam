const express = require("express");
const router = express.Router();
const articleControllers = require ("../controllers/articleControllers")

router.post("/new", (req, res) => {
    articleControllers.createArticle(req.body).then(resultFromController => res.send(resultFromController))
});

router.put("/:articleId/editArticle", (req, res) => {
    articleControllers.editArticle(req.params.articleId,req.body).then(resultFromController => res.send(resultFromController))
})

router.get("/", (req,res) =>{
    articleControllers.retrieveArticle().then(resultFromController => res.send(resultFromController))
})

router.delete("/:articleId/delete", (req,res) => {
    articleControllers.deleteArticle(req.params.articleId).then(resultFromController => res.send(resultFromController));
});
module.exports = router;