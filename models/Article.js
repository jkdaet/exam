const mongoose = require("mongoose");

const articleSchema = new mongoose.Schema({
    title:{
        type: String
    },
    content:{
        type: String
    },
    created:{
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Article", articleSchema);