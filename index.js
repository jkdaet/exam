const express = require("express");
const mongoose = require("mongoose");

const articleRoutes = require ("./routes/articleRoutes")

const api = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin@course-booking.cafudag.mongodb.net/exam?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;
db.once("error", console.error.bind(console, "Could not Connect"));

db.once("open", () => console.log("Server is connected to MongoDB."));

api.use(express.json());
api.use(express.urlencoded({extended: true}));

api.use("/articles", articleRoutes);

api.listen(process.env.PORT || port, () => {
    console.log(`API is now accessible in port ${process.env.PORT || port}`)
})